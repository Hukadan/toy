from django.conf.urls import include, url

from testdb.urls import router


urlpatterns = [
    url(r'^api/', include(router.urls)),
    ]
