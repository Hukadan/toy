from rest_framework import authentication, permissions, viewsets, filters

from .models import Client, Dossier
from .serializers import ClientSerializer, DossierSerializer
from .forms  import DossierFilter, ClientFilter

class ClientViewSet(viewsets.ModelViewSet):

    filter_backends = (                                                     
        filters.DjangoFilterBackend,
        filters.SearchFilter,
        filters.OrderingFilter,
        )

    queryset = Client.objects.order_by('name') 
    serializer_class = ClientSerializer
    filter_class = ClientFilter

class DossierViewSet(viewsets.ModelViewSet):

    filter_backends = (                                                     
        filters.DjangoFilterBackend,
        filters.SearchFilter,
        filters.OrderingFilter,
        )

    queryset = Dossier.objects.order_by('numero')
    serializer_class = DossierSerializer
    filter_class = DossierFilter


