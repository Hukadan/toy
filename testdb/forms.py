import django_filters

from .models import Dossier, Client


class DossierFilter(django_filters.FilterSet):

    class Meta:
        model = Dossier
        fields = ('client',)


class ClientFilter(django_filters.FilterSet):

    class Meta:
        model = Client
        fields = ('dossier',)

