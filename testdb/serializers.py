from rest_framework import serializers
from rest_framework.reverse import reverse                                  

from .models import Client, Dossier, Affectation

class ClientSerializer(serializers.ModelSerializer):

    class Meta:
        model = Client
        fields = ('name', )

class DossierSerializer(serializers.ModelSerializer):                            
    class Meta:
        model = Dossier
        fields = ('numero','client', )

