from django.contrib import admin

from .models import Client, Dossier, Affectation
# Register your models here.
class ClientAdmin(admin.ModelAdmin):
    pass
admin.site.register(Client, ClientAdmin)

class DossierAdmin(admin.ModelAdmin):
    pass
admin.site.register(Dossier, DossierAdmin)

class AffectationAdmin(admin.ModelAdmin):
    pass
admin.site.register(Affectation, AffectationAdmin)
