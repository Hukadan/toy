from __future__ import unicode_literals

from django.db import models

# Create your models here.

class Client(models.Model):
    
    # db_table = 'nom de la table'

    name = models.CharField(max_length=50) # db_column = 'colonne'

    def __str__(self):
        return self.name

class Dossier(models.Model):
    numero = models.CharField(max_length=50)
    client = models.ManyToManyField(
            Client,
            through='Affectation',
            through_fields=('dossier','client'),
            )
    def __str__(self):                                                          
        return self.numero  

class Affectation(models.Model):
    client = models.ForeignKey(Client, on_delete=models.CASCADE)
    dossier = models.ForeignKey(Dossier, on_delete=models.CASCADE)


